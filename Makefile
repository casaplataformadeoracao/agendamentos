export VERSION=latest
export IMAGE_NAME=registry.gitlab.com/casaplataformadeoracao/agendamentos

build:
	cd login/login-test && \
	npm install && \
	npm run build

run:
	cd login/login-test && \
	npx prisma migrate dev && \
	npm run dev

start:
	cd login/login-test && \
	npx prisma migrate deploy && \
	cd .next/standalone && \
	node server.js

copy:
	rsync -a login/login-test/.next/standalone /home/juliohm/Downloads/asdf/
	rsync -a login/login-test/.next/static /home/juliohm/Downloads/asdf/standalone/.next/
	rsync -a login/login-test/public /home/juliohm/Downloads/asdf/standalone/
	rsync -a login/login-test/prisma /home/juliohm/Downloads/asdf/standalone/

docker:
	docker build --progress plain -t $(IMAGE_NAME):$(VERSION) .

## Abaixo, usado para testes locais

dockerbash:
	docker run --rm -it -p3000:3000 -v ./login/login-test/.env:/opt/agendamentos/.env --entrypoint bash registry.gitlab.com/casaplataformadeoracao/agendamentos

dockerrun:
	docker run --rm -it -p3000:3000 -v ./login/login-test/.env:/opt/agendamentos/.env registry.gitlab.com/casaplataformadeoracao/agendamentos

clean:
	rm -fr login/login-test/node_modules
	rm -fr login/login-test/.next

mysqlcreate:
	docker run -d --name mysql-agendamentos -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=cpoagendamento mysql

mysqlstart:
	docker start mysql-agendamentos

mysqlstop:
	docker stop mysql-agendamentos

mysqllogs:
	docker logs -f mysql-agendamentos
