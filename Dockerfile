FROM node:21 as builder

ARG VERSAO=none

COPY login/login-test/ /opt/agendamentos/

WORKDIR /opt/agendamentos

RUN npm ci --only=production
RUN printf '{"versao": "%s"}' $VERSAO > src/app/version.json
RUN npm run build

## IMAGEM FINAL
FROM node:21

ENV NODE_ENV production

COPY --chown=node:node --from=builder /opt/agendamentos/.next/standalone/ /opt/agendamentos/
COPY --chown=node:node --from=builder /opt/agendamentos/.next/static/ /opt/agendamentos/.next/static/
COPY --chown=node:node --from=builder /opt/agendamentos/public/ /opt/agendamentos/public/
COPY --chown=node:node --from=builder /opt/agendamentos/prisma/ /opt/prisma/
COPY docker-entrypoint.sh /usr/local/bin/

RUN apt-get update && \
    apt-get install -y --no-install-recommends dumb-init

USER node

WORKDIR /opt/agendamentos

ENTRYPOINT [ "dumb-init", "/usr/local/bin/docker-entrypoint.sh" ]
