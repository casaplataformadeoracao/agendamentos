# Meet Our Team

---

## Alexandre Baiocco
![Alexandre Baiocco](link_para_foto_de_Alexandre_Baiocco.jpg)
- [LinkedIn](https://www.linkedin.com/in/alexandre-baiocco-432b261aa/)
- 📧: [emal@email.com](mailto:emal@email.com)

---

## Alexandre Mendes
![Alexandre Mendes](link_para_foto_de_Alexandre_Mendes.jpg)
- [LinkedIn](https://www.linkedin.com/in/alexandre-mendes-macedo-3513ab1b3/)
- 📧: [aleemendes.00@gmail.com](mailto:aleemendes.00@gmail.com)

---

## Carlos Eduardo Oliveira
![Carlos Eduardo Oliveira](link_para_foto_de_Carlos_Eduardo_Oliveira.jpg)
- [LinkedIn](https://www.linkedin.com/in/carlos-eduardo-mendonca/)
- 📧: [cadue84@gmail.com](mailto:cadue84@gmail.com)

---

## Emilio Buztlaff
![Emilio Buztlaff](link_para_foto_de_Emilio_Buztlaff.jpg)
- [LinkedIn](https://www.linkedin.com/in/emilio-butzlaff/)
- 📧: [emiliobutz@gmail.com](mailto:emiliobutz@gmail.com)

---

## Thiago Mendes
![Thiago Mendes](link_para_foto_de_Thiago_Mendes.jpg)
- [LinkedIn](https://www.linkedin.com/in/tlmendes/)
- 📧: [thimendes@outlook.com.br](mailto:thimendes@outlook.com.br)

