import React, { useCallback, useEffect, useState } from 'react'
import { ApiRequests, IState } from '@/app/utils/ApiRequests'
import './styles.css'
import { FieldErrors, FieldValues, UseFormRegister } from 'react-hook-form'
import { apiFromIBGE } from '@/app/lib/apiIbge'

export type Props<T extends FieldValues> = {
  register: UseFormRegister<T>,
  errors: FieldErrors<T>,
  watch: (field: keyof T) => T[keyof T],
  setValue: (field: keyof T, value: T[keyof T]) => void
  getValues: (field: keyof T) => T[keyof T]
}

/**
 * @param register
 * @param errors
 * @param watch
 * @param setValue
 * @returns
 * @description
 * This component is used to select the location of the user.
 * It uses the IBGE API to get the countries, states and cities.
 */
function LocationSelector<T extends FieldValues>({ register, errors, watch, setValue, getValues }: Props<T>) {
  const [countries, setCountries] = useState<string[]>([]);
  const [states, setStates] = useState<IState[]>([]);
  const [cities, setCities] = useState<string[]>([]);

  const [country, setCountry] = useState<string>('');
  const [state, setState] = useState<string>('');

  const getCountries = useCallback(async () => {
    const apiIbge = new ApiRequests(apiFromIBGE);
    const data = await apiIbge.getCountries();
    setCountries((prevCountries) => [...prevCountries, ...data]);;
  }, [])

  const getStates = useCallback(async () => {
    const apiIbge = new ApiRequests(apiFromIBGE);
    const data = await apiIbge.getStates();
    setStates((prevStates) => [...prevStates, ...data]);;
  }, [])

  const getCities = useCallback(async () => {
    try {
      const apiIbge = new ApiRequests(apiFromIBGE);
      const data = await apiIbge.getCities(state);
      setCities(data);
    }
    catch (e) {
      throw new Error('Erro ao buscar cidades');
    }

  }, [state])

  useEffect(() => {
    getCountries();

  }, [getCountries]);

  useEffect(() => {
    if (country === 'Brasil') {
      setValue("state", "" as unknown as T[keyof T]);
      setValue("city", "" as unknown as T[keyof T]);
      getStates();
    };
    setCities([]);
    setStates([]);

  }, [getStates, setValue, getValues, country]);

  useEffect(() => {
    getCities();

  }, [getCities, state]);

  return (
    <>
      <fieldset className='flex flex-col gap-4'>

        <div className='relative pb-4'>
          <label htmlFor='country'>País</label>
          <select
            id='country' {...register("country" as any, { required: true })}
            className='form-control'
            defaultValue={''}
            onChange={(e) => { setCountry(e.target.value) }}
          >
            <option value='' disabled>Selecione um País</option>
            {
              countries.map((country: string) => (
                <option key={country} value={country}>{country}</option>
              ))
            }
          </select>
          {errors.country && <span className='error-message'>Campo obrigatório</span>}
        </div>

        {
          states.length > 0 && (
            <div className='relative pb-4'>
              <label htmlFor='state'>Estado</label>
              <select
                id='state' {...register("state" as any, { required: true })}
                className='form-control'
                defaultValue={''}
                onChange={(e) => { setState(e.target.value) }}
              >
                <option value='' disabled>Selecione um Estado</option>
                {
                  states.map(({ id, sigla, nome }: IState) => (
                    <option key={id} value={sigla}>{nome}</option>
                  ))
                }
              </select>
              {errors.state && <span className='error-message'>Campo obrigatório</span>}
            </div>
          )
        }

        {
          cities.length > 0 && (
            <div className='relative pb-4'>
              <label htmlFor='city'>Cidade</label>
              <select
                id='city' {...register("city" as any, { required: true })}
                className='form-control'
                defaultValue={''}
              >
                <option value='' disabled>Selecione uma Cidade</option>
                {
                  cities.map((city: string) => (
                    <option key={city} value={city}>{city}</option>
                  ))
                }
              </select>
              {errors.city && <span className='error-message'>Campo obrigatório</span>}
            </div>
          )
        }

      </fieldset>
    </>
  )
}

export default LocationSelector;