"use client"

import React, { useState } from "react";
import { Button, Modal, Input, InputNumber, Space } from "antd";
import { ILocal } from "../TableLocais";
import { PenSquare } from "lucide-react";
import { api } from '@/app/lib/api';
import { localService } from "@/app/utils/LocalService";
import { useMutation, useQueryClient } from 'react-query';
import Swal from "sweetalert2";


interface Props {
  props: ILocal;
}

const ModalLocal: React.FC<Props> = ({ props }: Props) => {
  const queryClient = useQueryClient();

  const [open, setOpen] = useState(false);
  const [localData, setLocalData] = useState<ILocal>({ ...props });
  
  const showModal = () => {
    setOpen(true);
  };

  const handleChange = (value: number) => {
    setLocalData({ ...localData, number: value });
  };

 
  const { mutate: handleSubmit, isLoading } = useMutation({
    mutationFn: async () => {
      const service = new localService(api);
      await service.updateLocal(localData);
    },
    onSuccess: () => {
      Swal.fire('Atualizado com Sucesso!');
      queryClient.invalidateQueries(['get_locals']);
      setOpen(false);
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo deu errado!',
      })
    }
  });

  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <>
      <button onClick={showModal}>
        <PenSquare size={18} stroke='green' />
      </button>
      <Modal
        open={ open }
        title={ `Editando Local ${props.localName}`}
        onOk={ handleCancel }
        onCancel={ handleCancel }
        footer={[
          <Button key="back" 
            danger
            onClick={ handleCancel }>
            Cancelar
          </Button>,
          <Button
            key="submit"
            type="primary"
            className={"bg-blue-700 text-white"}
            loading={ isLoading }
            onClick={ () => handleSubmit() }
          >
            Atualizar
          </Button>
        ]}
      >
      <>
        <Space.Compact
          style={{
            width: '100%',
          }}
          >
          <Input
              className="border-blue-500" type="text" style={{
                width: '80%',
              }}
              value={localData.localName}
              onChange={(e) => setLocalData({ ...localData, localName: e.target.value })}
          />
          <Input type="text" style={{
            width: '20%',
          }} 
          count={{
            show: true,
            max: 4
          }}
              value={localData.abbreviation}
              onChange={(e) => setLocalData({ ...localData, abbreviation: e.target.value })}

            />
        </Space.Compact>
          <Input type="text"
            value={localData.country}
            onChange={(e) => setLocalData({ ...localData, country: e.target.value })}

          />
        <Space.Compact
          style={{
            width: '100%',
          }}
        >
          <Input type="text" style={{
            width: '50%',
          }} 
              value={localData.state}
              onChange={(e) => setLocalData({ ...localData, state: e.target.value })}

            />
          <Input type="text" style={{
            width: '50%',
          }} 
          value={ localData.city } />
        </Space.Compact>
        <Space.Compact
        style={{
          width: '100%',
        }}
      >
           <Input type="text" style={{
              width: '80%',
            }}
              value={localData.street}
              onChange={(e) => setLocalData({ ...localData, street: e.target.value })}

            />
            <InputNumber
              type="number"
              style={{ width: '20%' }}
              min={ 0 }
              value={ localData.number }
                onChange={ handleChange as (value: number | null) => void }
            />
          </Space.Compact>
        </>            
      </Modal>
    </>
  );
};

export default ModalLocal;
