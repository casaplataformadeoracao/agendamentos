'use client';

import { SideMenuContext } from '@/app/contexts/SideMenuContext';
import { ReactNode, useContext } from 'react';

interface IRenderPages {
  children: ReactNode;
}

function RenderPages({ children }: IRenderPages) {
  const { isOpen } = useContext(SideMenuContext);

  return (
    <div className={`flex justify-center items-center p-4
    ${isOpen ? 'md:ml-[13.225rem]' : 'md:ml-[4.225rem]'} mt-[4.525rem]
     transition-all duration-300 ease-in-out`}>
      {children}
    </div>
  )
}

export default RenderPages