import Link from 'next/link';

type Props = {
  title: string;
  className?: string;
  color: string;
  href: string;
};

const CardLinks = (props: Props): JSX.Element => {
  return (
    <>
      <Link
        className={`text-xl font-bold font-sans text-[#636363]
            border-none truncate sm:text-center`}
        href={props.href}
      >
        <div
          className={`w-[20.25rem] truncate p-2 rounded-lg shadow-lg
            ${props.color} sm:h-36 flex items-center justify-center h-16`}
        >
          <h2 className='text-center truncate'>{props.title}</h2>
        </div>
      </Link>
    </>
  );
};

export default CardLinks;
