import { XCircle } from 'lucide-react';
import React from 'react';
import Button from '../Button';
import ModalLocal from '../Modal';

type Props = {
  data: ILocal[],
  handleDelete: (id: number) => void,
}
export type ILocal = {
  id: number;
  localName: string;
  street: string;
  number: number;
  city: string;
  state: string;
  abbreviation: string;
  country: string
}

const DataTable = ({ data, handleDelete }: Props) => {
  return (
    <table className="w-full text-sm text-left">
      <thead>
        <tr className="bg-gray-200">
          <th className="py-2 px-2 max-w-[12rem] truncate">Sigla</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Nome</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Endereço</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Número</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Cidade</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Estado</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">País</th>
          <th className="py-2 px-2 max-w-[3rem] truncate text-center">Editar/Apagar</th>
        </tr>
      </thead>
      <tbody className='text-xs'>
        {data.map((item, index) => (
          <tr
            key={item.id}
            className={`${index % 2 === 0 ? 'bg-gray-300' : 'bg-white'}`}
          >
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.abbreviation}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.localName}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{`${item.street}`}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.number}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.city}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.state}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{item.country}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate text-center flex gap-2 justify-center">
              <ModalLocal key={item.id} props={item} />
              <Button
                onClick={() => handleDelete(item.id)}
              >
                <XCircle size={18} stroke='red' />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default DataTable;