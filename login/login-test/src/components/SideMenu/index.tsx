'use client';

import { SideMenuContext } from '@/app/contexts/SideMenuContext';
import { LogOut, Menu, MoreVertical } from 'lucide-react';
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { ReactNode, useContext } from 'react';
import AvatarLogin from '../AvatarLogin';
import SideMenuItem from '../SideMenuItem';

export interface ISideMenu {
  children: ReactNode;
}

/**
 * Componente responsável por criar o menu lateral
 * @param children Componentes filhos do menu
 * @returns Componente de menu lateral
 * @example
 * <SideMenu>
 *  <SideMenuItem icon={<Home size={20} />} text='Início' href='/' />
 * </SideMenu>
 */
export default function SideMenu({ children }: ISideMenu) {
  const { isOpen, setIsOpen } = useContext(SideMenuContext);

  const handleOpen = () => {
    setIsOpen(!isOpen);
  }

  const { data: session } = useSession();
  const imageAvatar = session?.user?.avatar || session?.user?.picture;

  const router = useRouter();

  const handleOptions = () => {
    router.push('/profile')
    setIsOpen(false);
  }

  const handleLogout = () => {
    signOut();
  }

  return (
    <aside className={
      `h-full fixed z-[1001] top-0 transition-transform duration-300 ease-in-out 
      ${isOpen ? 'w-[13.225rem]' : '-translate-x-32 md:-translate-x-0 opacity-0 md:opacity-100 w-[4.25rem] md:block'}`
    }>
      <nav className={`relative h-screen flex flex-col bg-gray-800
      ${isOpen ? 'shadow-md' : ''}`}>
        <div
          id='header-nav'
          className={`flex justify-startitems-center p-4 h-[4.525rem] min-h-[4.525rem]`}
        >
          <button
            onClick={() => handleOpen()}
            className={`cursor-pointer transition-colors duration-300 ease-in-out`}
          >
            <div className='p-2 rounded-full hover:bg-gray-400 font-medium text-gray-50 flex items-center'>
              <Menu size={20} />
            </div>
          </button>
        </div>
        <div className='flex flex-1 flex-col justify-between'>
          <div className='flex flex-col h-full justify-between m-2'>
            <ul className='flex flex-col gap-1 border-b-[1px] border-gray-300 pb-2'>
              {children}
            </ul>
            {session && (
              <ul className='flex flex-col gap-1'>
                <button type='button' onClick={handleLogout}>
                  <SideMenuItem icon={<LogOut size={20} />} text='Sair' href='/' />
                </button>
              </ul>
            )}
          </div>
        </div>
        <div id='footer-nav'>
          {session && (
            <>
              <div className={`flex gap-1 ${isOpen ? 'justify-start' : 'justify-center'} items-center p-2 border-t-[1px] border-gray-300`}>
                {imageAvatar ? (
                  <Image
                    src={imageAvatar}
                    alt='Avatar'
                    className='rounded-lg'
                    width={40}
                    height={40}
                  />
                ) : (
                  <AvatarLogin />
                )}
                <div className={`${isOpen ? "" : "hidden"} flex flex-1 items-center justify-between truncate`}>
                  {session.user.name && (
                    <div className='flex gap-1 w-full'>
                      <div className='w-full truncate text-gray-50'>
                        <p className='text-xs font-semibold truncate'>{session.user.name}</p>
                        <p className='text-xs font-normal truncate'>{session.user.email}</p>
                      </div>
                      <button type='button' onClick={handleOptions}>
                        <MoreVertical className='text-gray-50' />
                      </button>
                    </div>
                  )}
                </div>
              </div>
            </>
          )}
        </div>
      </nav>
    </aside>
  )
}


