import { IEvent } from '@/app/(user-pages)/agendamentos/page';
import { Button } from 'antd';
import { CalendarPlus, Loader } from 'lucide-react';
import { useSession } from 'next-auth/react';
import React, { useEffect, useState } from 'react'

interface Props {
  Events: any;
  setReminder: (task: IEvent) => void;
  isUserRegistered: (event: IEvent) => boolean;
  isLoading: boolean;
}

const Schedules = ({ Events, setReminder, isUserRegistered, isLoading }: Props) => {
  return (
    <div className='mt-8'>
      <table
        className='mx-auto w-3/4 text-sm text-left border-collapse border border-gray-300'
      >
        <thead>
          <tr>
            <th className='p-2 border-collapse border border-gray-300'>Nome</th>
            <th className='p-2 border-collapse border border-gray-300'>Descrição</th>
            <th className='p-2 border-collapse border border-gray-300'>Local</th>
            <th className='p-2 border-collapse border border-gray-300'>Data</th>
            <th className='p-2 border-collapse border border-gray-300'>Vagas</th>
            <th className='p-2 border-collapse border border-gray-300 text-center'>Agendar</th>
          </tr>
        </thead>
        <tbody>
          {Events.length !== 0 ? (
            Events.map((event: IEvent) => (
              <tr key={event.id}>
                <td className='p-2 border-collapse border border-gray-300'>{event.title}</td>
                <td className='p-2 border-collapse border border-gray-300'>{event.description}</td>
                <td className='p-2 border-collapse border border-gray-300'>{event.local.localName}</td>
                <td className='p-2 border-collapse border border-gray-300'>{new Date(event.date).toLocaleDateString()}</td>
                <td className='p-2 border-collapse border border-gray-300'>{event.spots}</td>
                <td className='text-center mx-auto p-2 border border-gray-300'>
                  {isLoading ? (
                      <Button disabled>
                        <Loader size={18} />
                      </Button>
                  ) : (
                    <Button disabled={isUserRegistered(event)} onClick={() => setReminder(event)}>
                      <CalendarPlus size={18}  />
                    </Button>
                  )}
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td className='p-2 border-collapse border border-gray-300 text-center' colSpan={6}>Nenhum evento encontrado</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

export default Schedules;
