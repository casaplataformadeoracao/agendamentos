import { XCircle } from 'lucide-react';
import Button from '../Button';

type Props = {
  data: IEvents[];
  handleDelete: (id: number) => void;
};

export interface IEvents {
  id: number;
  title: string;
  description: string;
  date: string;
  bookings: number;
  spots: number;
  localEventAbbreviation: string;
}

const TableEvents = ({ data, handleDelete }: Props) => {
  const events = data.map((evento) => {
    const formatedData = new Date(evento.date).toLocaleDateString('pt-BR', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    });
    const newEvent = { ...evento, formatedData };
    return newEvent;
  });
  return (
    <table className='w-full text-sm text-left'>
      <thead>
        <tr className='bg-gray-200'>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Titúlo</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Descrição</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Data</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Vagas</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Rerservas</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Local</th>
          <th className='py-2 px-2 max-w-[12rem] truncate'>Vagas Restantes</th>
          <th className='py-2 px-2 max-w-[3rem] truncate text-center'>
            Editar/Apagar
          </th>
        </tr>
      </thead>
      <tbody className='text-xs'>
        {events.map((item, index) => (
          <tr
            key={item.id}
            className={`${index % 2 === 0 ? 'bg-gray-300' : 'bg-white'}`}
          >
            <td className='py-1 px-1 max-w-[12rem] truncate'>{item.title}</td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>
              {item.description}
            </td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>{item.formatedData}</td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>{item.spots}</td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>
              {item.bookings}
            </td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>
              {item.localEventAbbreviation}
            </td>
            <td className='py-1 px-1 max-w-[12rem] truncate'>
              {item.spots - item.bookings}
            </td>
            <td className='py-1 px-1 max-w-[12rem] truncate text-center flex gap-2 justify-center'>
              {/* <ModalUser key={item.id} props={item} /> */}
              <Button onClick={() => handleDelete(item.id)}>
                <XCircle size={18} stroke='red' />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TableEvents;
