'use client';

import { SideMenuContext } from '@/app/contexts/SideMenuContext';
import Link from 'next/link';
import { useContext } from 'react';

interface IMenuItem {
  icon: any;
  text: string;
  active?: boolean;
  href: string;
}

/**
 * Componente responsável por criar um item do menu
 * @param icon Icone do item do menu
 * @param text Texto do item do menu
 * @param active Se o item está ativo
 * @param href Link para onde o item do menu vai
 * @returns Componente de item do menu
 * @example
 * <SideMenuItem icon={<Home size={20} />} text='Início' href='/' />
 */
export default function SideMenuItem({ icon, text, active, href }: IMenuItem) {
  const { isOpen, setIsOpen } = useContext(SideMenuContext);

  return (
    <Link href={href}>
      <li
        className={`relative flex items-center py-2 px-[0.885rem] gap-2
      rounded-lg cursor-pointer text-sm font-medium
      text-indigo-50 hover:text-gray-200 hover:bg-indigo-400
      transition-all duration-300 ease-in-out overflow-hidden
      ${isOpen ? `justify-start` : `justify-center`}
      ${active ? `` : ``}`}
        // onClick={() => setIsOpen(false)}
      >
        {icon}
        <p
          className={`${
            isOpen ? 'block' : 'md:absolute md:translate-x-32'
          } text-justify truncate overflow-hidden`}
        >
          {text}
        </p>
      </li>
    </Link>
  );
}
