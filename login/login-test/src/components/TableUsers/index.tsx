import { XCircle } from 'lucide-react';
import React from 'react';
import Button from '../Button';
import ModalUser from '../ModalUser';

type Props = {
  data: IUser[],
  handleDelete: (id: number) => void,
}

export type IUser = {
  id: number;
  email: string;
  name: string;
  phone: string;
  country: string;
  city: string;
  state: string;
  isAdmin: boolean;
  verified: boolean
}

const TableUsers = ({ data, handleDelete }: Props) => {
  return (
    <table className="w-full text-sm text-left">
      <thead>
        <tr className="bg-gray-200">
          <th className="py-2 px-2 max-w-[12rem] truncate">Nome</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">E-mail</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Telefone</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">País</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Estado</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Cidade</th>
          <th className="py-2 px-2 max-w-[12rem] truncate">Verificado ?</th>
          <th className="py-2 px-2 max-w-[3rem] truncate text-center">Editar/Apagar</th>
        </tr>
      </thead>
      <tbody className='text-xs'>
        {data.map((user, index) => (
          <tr
            key={user.id}
            className={`${index % 2 === 0 ? 'bg-gray-300' : 'bg-white'}`}
          >
            <td className="py-1 px-1 max-w-[12rem] truncate">{user.name}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{user.email}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{`${user.phone}`}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{user.country}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{user.state}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">{user.city}</td>
            <td className="py-1 px-1 max-w-[12rem] truncate">
              {user.verified ? 'Sim' : 'Não'}
            </td>
            <td className="py-1 px-1 max-w-[12rem] truncate text-center flex gap-2 justify-center">
              <ModalUser key={user.id} user={user} />
              <Button
                onClick={() => handleDelete(user.id)}
              >
                <XCircle size={18} stroke='red' />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TableUsers;