'use client';

import { SideMenuContext } from '@/app/contexts/SideMenuContext';
import AvatarLogin from '@components/AvatarLogin';
import Button from '@components/Button';
import { Menu } from 'lucide-react';
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { useContext } from 'react';

const AppBar = () => {
  const { data: session } = useSession();
  const imageAvatar = session?.user?.avatar || session?.user?.picture;
  const router = useRouter();

  const { isOpen, setIsOpen } = useContext(SideMenuContext);

  const handleOpenMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <header className='flex justify-between items-center bg-gray-800 p-2 absolute top-0 z-[1000] w-full h-[4.525rem] max-h-[4.525rem] shadow-md truncate'>
      <div>
        <button
          onClick={() => handleOpenMenu()}
          className={`flex cursor-pointer transition-colors duration-300 ease-in-out`}
        >
          <div className='py-2 px-2 rounded-full hover:bg-gray-800 font-medium text-gray-50 flex justify-start items-center'>
            <Menu size={20} />
          </div>
        </button>
      </div>
      <div className='flex justify-end items-center text-sm gap-4 truncate'>
        {!session?.user ? (
          <>
            <Button
              onClick={() => router.push('/auth/signin')}
              className={`
              border-[1px] border-blue-50 rounded-md bg-blue-500 font-semibold px-3 py-2 text-base text-blue-50
              hover:bg-blue-50 hover:border-blue-600 hover:text-blue-500 transition-all duration-300 ease-in-out
              `}
            >
              Entrar
            </Button>
          </>
        ) : (
          <>
            <p className='text-md text-center truncate text-gray-50 font-semibold'>
              Bem vindo, {session?.user.name}
            </p>
            <Button
              onClick={() => signOut()}
              className={`
              border-[1px] border-red-800 rounded-md bg-red-500 font-semibold px-3 py-2 text-base text-gray-50
              hover:bg-red-800 hover:border-red-500 hover:text-gray-50 transition-all duration-300 ease-in-out
              `}
            >
              Sair
            </Button>
            <Button className='min-w-[2.5rem]'>
              {imageAvatar ? (
                <Image
                  src={imageAvatar}
                  alt='Avatar'
                  className='rounded-full'
                  width={40}
                  height={40}
                />
              ) : (
                <AvatarLogin />
              )}
            </Button>
          </>
        )}
      </div>
    </header>
  );
};

export default AppBar;
