import React, { InputHTMLAttributes } from 'react';

const Input = (props: InputHTMLAttributes<HTMLInputElement>) => {
  return (
    <div className='relative mt-2'>
      <input
        {...props}
        className='peer w-full px-4 py-3 text-gray-700
        rounded-xl shadow-inner border-none outline-none
      bg-gray-200 outline-2 focus:outline-blue-500
      placeholder-shown:bg-gray-200 placeholder-shown:text-white
        placeholder-transparent mb-4'
        size={40}
      />
      <label
        className='absolute left-4 -top-6 px-1 text-sm opacity-0
          transition-all bg-transparente text-blue-500 rounded font-bold
          peer-placeholder-shown:text-sm peer-placeholder-shown:text-gray-400
          peer-placeholder-shown:top-3.5 peer-placeholder-shown:left-4 peer-placeholder-shown:bg-transparent
          peer-placeholder-show:bg-gray-200 peer-focus:-top-6 peer-focus:left-4 peer-placeholder-shown:font-normal
        peer-focus:text-blue-500 peer-focus:font-semibold peer-placeholder-shown:opacity-100 peer-focus:opacity-100'
        htmlFor={props.id}
      >
        {props.placeholder}
      </label>
    </div>
  );
};

export default Input;
