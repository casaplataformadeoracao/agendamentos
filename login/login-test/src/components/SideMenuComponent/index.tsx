'use client';

import {
  CalendarCheck,
  CalendarDaysIcon,
  CalendarSearch,
  Home,
  Hotel,
  Lightbulb,
  Search,
  Users,
} from 'lucide-react';
import { useSession } from 'next-auth/react';
import SideMenu from '../SideMenu';
import SideMenuItem from '../SideMenuItem';

const SideMenuComponent = () => {
  const { data: session } = useSession();

  return (
    <>
      <SideMenu>
        <SideMenuItem
          icon={<Home size={20} />}
          text='Ir para Home'
          href='/home'
        />
        {session && (
          <>
            <SideMenuItem
              icon={<CalendarSearch size={20} />}
              text='Painel de Controle'
              href='/'
            />
            {session?.user?.isAdmin ? (
              <>
                <SideMenuItem
                  icon={<CalendarDaysIcon size={20} />}
                  text='Cadastrar Eventos'
                  href='/cadastrar/eventos'
                />
                <SideMenuItem
                  icon={<Search size={20} />}
                  text='Visualizar Eventos'
                  href='/listar/eventos'
                />
                <SideMenuItem
                  icon={<Hotel size={20} />}
                  text='Cadastrar Locais'
                  href='/cadastrar/locais'
                />
                <SideMenuItem
                  icon={<Search size={20} />}
                  text='Visualizar Locais'
                  href='/listar/locais'
                />
                <SideMenuItem
                  icon={<Users size={20} />}
                  text='Visualizar Usuários'
                  href='/listar/usuarios'
                />
              </>
            ) : (
              <>
                <SideMenuItem
                  icon={<CalendarCheck size={20} />}
                  text='Agendamentos'
                  href='/agendamentos'
                />
                <SideMenuItem
                  icon={<Lightbulb size={20} />}
                  text='Instruções'
                  href='/instrucoes-evento'
                />
              </>
            )}
          </>
        )}
      </SideMenu>
    </>
  );
};

export default SideMenuComponent;
