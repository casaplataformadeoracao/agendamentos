import React, { useState } from 'react';
import {
  Button, Checkbox, Col,
  Drawer, Form, Input, Row,
  Select, Space
} from 'antd';
import { PenSquare } from 'lucide-react';
import { IUser } from '@components/TableUsers'
import Swal from 'sweetalert2';
import { UserService } from '@/app/utils/UserService';
import { useMutation, useQueryClient } from 'react-query';
import { api } from '@/app/lib/api';

const { Option } = Select;


interface Props {
  user: IUser
}

const ModalUser: React.FC<Props> = ({ user }: Props) => {
  const [open, setOpen] = useState(false);
  const queryClient = useQueryClient();
  const [userEdited, setUserEdited] = useState<IUser>({ ...user });

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const { mutate: handleSubmit, isLoading } = useMutation({
    mutationFn: async () => {
      const service = new UserService(api);
      const { id, ...userUpdated } = userEdited;
      await service.updateUser(user.id, userUpdated);
    },
    onSuccess: () => {
      Swal.fire('Editado com Suceso!');
      queryClient.invalidateQueries(['get_users']);
      onClose();
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',
      })
      onClose();
    },
  });

  return (
    <>
      <button onClick={ showDrawer }>
        <PenSquare size={18} stroke='green' />
      </button>
      <Drawer
        title={`Editando usuário: ${ user.name }`}
        width={640}
        onClose={onClose}
        open={open}
        styles={{
          body: {
            paddingBottom: 80,
          },
        }}
        extra={
          <Space>
            <Button key="back" 
              danger
              onClick={ onClose }>
              Cancelar
            </Button>,
            <Button
              key="submit"
              type="primary"
              className={"bg-blue-700 text-white"}
              onClick={ () => handleSubmit() }
              loading={ isLoading }
            >
              Atualizar
            </Button>
          </Space>
        }
      >
        <Form
          layout="vertical"
          
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Nome completo"
                rules={[{ required: true, message: 'Nome é obrigatório' }]}
                
              >
                <Input
                  placeholder="Por favor, entre com seu nome"
                  defaultValue={user.name}
                  value={userEdited.name}
                  onChange={ (e) => setUserEdited({ ...userEdited, name: e.target.value }) }
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="email"
                label="E-mail"
                rules={[{ required: true, message: 'E-mail é obrigatório' }]}
              >
                <Input
                  style={{ width: '100%' }}
                  placeholder="Por favor, entre com seu e-mail"
                  defaultValue={user.email}
                  value={userEdited.email}
                  onChange={ (e) => setUserEdited({ ...userEdited, email: e.target.value }) }
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="country"
                label="País"
                rules={[{ required: true, message: 'Por favor, escolha um país' }]}   
              >
                <Select placeholder="Por favor, escolha seu país" defaultValue={user.country}>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="state"
                label="Estado"
                rules={[{ required: true, message: 'Estado é obrigatório quando País é Brasil' }]}
              >
                <Select placeholder="Por favor, escolha um Estado" defaultValue={ user.state }>
                  <Option value="private">Private</Option>
                  <Option value="public">Public</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="city"
                label="Cidade"
                rules={[{ required: true, message: 'Cidade é obrigatório quando País é Brasil' }]}
              > 
                <Select placeholder="Please choose the approver" defaultValue={user.city}>
                  <Option value="jack">Jack Ma</Option>
                  <Option value="tom">Tom Liu</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone"
                label="Telefone"
                rules={[{ required: true, message: 'Telefone é obrigatório' }]}
              >
                <Input
                  style={{ width: '100%' }}
                  placeholder='Por favor, entre com seu telefone'
                  defaultValue={user.phone}
                  value={userEdited.phone}
                  onChange={ (e) => setUserEdited({ ...userEdited, phone: e.target.value }) }
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12} justify="start">
            <Col span={8}>
              <Form.Item
                name="isAdmin"
                label="Administrador ?"
              >
                <Checkbox
                  defaultChecked={user.isAdmin}
                  checked={userEdited.isAdmin}
                  onChange={ (e) => setUserEdited({ ...userEdited, isAdmin: !userEdited.isAdmin }) }
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12} justify="start">
            <Col span={8}>
              <Form.Item
                name="verified"
                label="Verificado ?"
              >
                <Checkbox
                  defaultChecked={user.verified}
                  checked={userEdited.verified}
                  onChange={ (e) => setUserEdited({ ...userEdited, verified: !userEdited.verified }) }
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </>
  );
};

export default ModalUser;