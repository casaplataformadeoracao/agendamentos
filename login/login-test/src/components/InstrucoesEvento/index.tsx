import { ReactNode } from 'react';

interface ChildrenNode {
  children: ReactNode
}

const ComponentInstrucoes = ({ children }: ChildrenNode) => {
  return (
    <>
      <p>{children}</p>
      <br />
    </>
  );
};

export default ComponentInstrucoes;
