'use client';

import React, { useContext } from 'react';
import Image from 'next/image';
import logo from '@assets/images/logo_cpo.jpg';
import instagram from '@assets/images/instagram.png';
import youtube from '@assets/images/youtube.png';
import telegram from '@assets/images/telegram2.png';
import { SideMenuContext } from '../contexts/SideMenuContext';

const Home = () => {
  const { isOpen } = useContext(SideMenuContext);

  return (
    <main className={`flex flex-col items-center justify-center w-full h-full`}>
      <div className='flex justify-center'>
        <Image src={logo} alt="Logo CPO" className='w-32 h-32'></Image>
      </div>
      <div className='flex justify-center flex-col items-center'>
        <br />
        <h1 className='text-2xl text-center'>Casa Plataforma de Oração</h1>
        <br />
        <p className='text-center'>Seja bem vindo ao agendamento para participar das reuniões.</p>
        <p className='text-center'>Faça seu cadastro e agende a sua visita para nos conhecer!</p>
        <br />
        <br />
        <div className='flex items-baseline'>
          <a href="https://www.instagram.com/stories/casaplataformadeoracao" target='_blank'>
            <Image src={instagram} alt="Instagram CPO" className='w-16 h-16'></Image>
          </a>
          <a href="https://t.me/casaplataformadeoracaorio " target='_blank'>
            <Image src={telegram} alt="Telegram CPO" className='w-16 h-16'></Image>
          </a>
          <a href="https://www.youtube.com/@casaplataformadeoracao" target='_blank'>
            <Image src={youtube} alt="Youtube CPO" className='w-16 h-16'></Image>
          </a>
        </div>
      </div>
    </main>
  )
}

export default Home