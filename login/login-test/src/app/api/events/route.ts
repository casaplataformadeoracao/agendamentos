'use server';

import { LocalEvent } from "@prisma/client";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";
import { authOptions } from "../auth/[...nextauth]/route";
import { controller } from "../controllers/eventController";
import { IUser } from "../controllers/userController";

export interface IEvent {
  id: number;
  title: string;
  description: string;
  spots: number;
  date: Date;
  bookings: number;
  local: LocalEvent;
  participants: IUser[];
  localEventAbbreviation: string;
}

export async function GET(_req: NextRequest) {
  const session = await getServerSession(authOptions);

  if (!session) {
    return NextResponse.json({ message: "Unauthorized" }, { status: 401 });
  }

  const { data, status } = await controller.getEvents();

  return NextResponse.json(data, { status });
}

export async function POST(req: NextRequest) {
  const session = await getServerSession(authOptions);

  if (!session?.user.isAdmin) {
    return NextResponse.json({ message: "Unauthorized" }, { status: 401 });
  }


  const body: IEvent = await req.json();

  const { data, status } = await controller.createEvent(body);

  return NextResponse.json(data, { status });
}
