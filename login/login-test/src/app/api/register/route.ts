import { NextRequest, NextResponse } from "next/server";
import { controller } from "../controllers/userController";

export async function POST(request: NextRequest) {
  const body = await request.json();
  const { data, status } = await controller.postUser(body);
  return NextResponse.json(data, { status });
}