'use server';

import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";
import { authOptions } from "../auth/[...nextauth]/route";
import { controller } from "../controllers/localController";


export interface ILocal {
  name: string,
  address: string,
  addressNumber: string,
  city: string,
  state: string,
  country: string,
  abbreviation: string,
}

export async function POST(req: NextRequest) {
  const session = await getServerSession(authOptions)
  if (!session?.user.isAdmin) {
    return NextResponse.json({ message: 'Unauthorized' }, { status: 401 });
  }
  const body: ILocal = await req.json();
  const { data, status } = await controller.postLocal(body);

  return NextResponse.json(data, { status });
}

export async function GET(_req: NextRequest) {
  const session = await getServerSession(authOptions)
  if (!session) {
    return NextResponse.json({ message: 'Unauthorized' }, { status: 401 });
  }
  const { data, status } = await controller.getLocal();
  return NextResponse.json(data, { status });
}