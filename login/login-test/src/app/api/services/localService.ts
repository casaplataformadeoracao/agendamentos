import { prisma } from "../lib/prisma";
import { ILocal } from "../local/route";
import { ILocal as UpdateILocal } from "@components/TableLocais"
import { z } from "zod"


const localSchema = z.object({
  id: z.number().optional(),
  localName: z.string(),
  street: z.string(),
  number: z.coerce.string(),
  city: z.string().nullable(),
  state: z.string().nullable(),
  country: z.string(),
  abbreviation: z.string(),
});

export type TLocal = z.infer<typeof localSchema>


const postLocal = async (body: ILocal) => {
  const { name, address, addressNumber, city, state, abbreviation, country } = body;
  const newPost = await prisma.localEvent.create({
    data: {
      localName: name,
      street: address,
      number: addressNumber,
      abbreviation,
      city,
      state,
      country,
    },
  });

  return newPost;
}

const getLocal = async () : Promise<TLocal[]> => {
  const local = await prisma.localEvent.findMany();
  return local;
}

const deleteLocal = async (id: string) => {
  const local = await prisma.localEvent.delete({
    where: {
      id: Number(id),
    },
  });

  return { data: local, status: 200 };
}

async function updateLocal(body: TLocal, id: string) {
  const result = localSchema.safeParse(body)
  if (!result.success) {
    return null;
  }
  const { localName, street, number, city, state, abbreviation, country } = result.data;
  const local = await prisma.localEvent.update({
    where: {
      id: Number(id),
    },
    data: {
      localName,
      street,
      number,
      abbreviation,
      city,
      state,
      country,
    },
  });

  return local;
}

export const service = { postLocal, getLocal, deleteLocal, updateLocal };