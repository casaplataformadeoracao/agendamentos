import { IUserWithouPassword } from '@/app/utils/UserService';
import { IUser } from '../controllers/userController';
import { prisma } from '../lib/prisma';
import * as bcrypt from 'bcryptjs';

const getUser = async (email: string, password: string) => {
  const user = await prisma.user.findUnique({
    where: {
      email,
    },
  });
  if (user && bcrypt.compareSync(password, user?.password)) {
    return user;
  }
  return null;
};

const findAllUsers = async () => {
  const users = await prisma.user.findMany();
  const userWithoutPassword = users.map((user) => {
    const { password, createdAt, updatedAt, avatar, ...restUser } = user;
    return restUser;
  })
  return userWithoutPassword;
};

const getUserByEmail = async (email: string) => {
  const userRegister = await prisma.user.findUnique({
    where: { 
      email,
    },
  });
  if (userRegister) {
    const { password, createdAt, updatedAt, avatar, ...user } = userRegister;
    return { data: user, status: 200 };
  }
  return { data: { message: 'User not Found' }, status: 404 };
};

const postUser = async (body: IUser) => {
  const { password } = body;
  const passwordEncrypt = bcrypt.hashSync(password, 10);
  try {
    const user = await prisma.user.create({
        data: {
          ...body,
          password: passwordEncrypt,
        },
      });
    return { data: user, status: 201 };
  } catch (error) {
    return { data: { message: 'User alread exists' }, status: 400 };
  }
}

const deleteUser = async (id: string) => {
  
  const deleted = await prisma.user.delete({
    where: {
      id: Number(id),
    },
  })
  return deleted;
}

const updateUser = async (id: string, body: IUserWithouPassword) => {
  const user = await prisma.user.update({
    where: {
      id: Number(id),
    },
    data: {
      ...body,
    },
  });
  return user;
}

const service = {
  getUser, findAllUsers,
  getUserByEmail, postUser,
  deleteUser, updateUser
};

export default service;
