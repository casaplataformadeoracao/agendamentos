import { LocalEvent } from "@prisma/client";
import { IEvent } from "../events/route";
import { prisma } from "../lib/prisma";

interface IUserOnlyID {
  id: number;
}

interface IEventWithID {
  id: number;
  title: string;
  description: string;
  spots: number;
  date: Date;
  bookings: number;
  local: LocalEvent;
  participants: [{ id: number }];
  localEventAbbreviation: string;
}

const getEvents = async () => {
  const events = await prisma.event.findMany({
    include: {
      local: true,
      participants: { select: { id: true } },
    },
  });

  return { data: events, status: 200 };
}

const createEvent = async (body: IEvent) => {
  const { title, description, spots, date = new Date(), bookings, local, localEventAbbreviation } = body;

  const newEvent = await prisma.event.create({
    data: {
      title,
      description,
      spots,
      date,
      bookings,
      local: {
        connect: {
          abbreviation: localEventAbbreviation,
        },
      },
    },
  });

  const localEvent = await prisma.localEvent.findUnique({
    where: {
      abbreviation: localEventAbbreviation,
    },
    include: {
      Event: true,
    },
  });

  const eventIds = localEvent?.Event.map((event) => ({ id: event.id })) || [];

  await prisma.localEvent.update({
    where: {
      abbreviation: localEventAbbreviation,
    },
    data: {
      Event: {
        connect: [...eventIds, { id: newEvent.id }],
      },
    },
  });

  return { data: newEvent, status: 200 };
}

const deleteEvent = async (id: string) => {
  try {
    const event = await prisma.event.delete({
      where: {
        id: Number(id),
      },
    });
    return event;
  } catch (error) {
    return null;
  }
}

const updateEvent = async (eventId: string, userId: IUserOnlyID) => {  
  const { data: events } = await getEvents();
  const isUserRegistered = events.some((event) => event.id === Number(eventId) && event.participants.some((participant) => participant.id === Number(userId)));
  
  if (isUserRegistered) {
    return { data: 'Usuário já registrado', status: 400 };
  }

  await prisma.event.update({
    where: {
      id: +eventId
    },
    data: {
      spots: { decrement: 1 },
      participants: {
        connect: {
          id: Number(userId)
        },
      },
    },
  });

  return { data: '', status: 203 };
  }

export const service = { getEvents, createEvent, deleteEvent, updateEvent };