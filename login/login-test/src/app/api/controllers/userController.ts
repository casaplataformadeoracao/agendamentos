import { IUserWithouPassword } from '@/app/utils/UserService';
import service from '../services/userService';

interface RequestBody extends email {
  password: string;
}

interface email {
  email: string;
}

export interface IUser {
  name: string;
  email: string;
  password: string;
  phone: string;
  city: string;
  country: string;
  state: string;
}

const getUser = async ({ email, password }: RequestBody) => {
  const user = await service.getUser(email, password);
  return user;
};

const getUserByEmail = async ({ email }: email) => {
  const user = await service.getUserByEmail(email);
  return user;
};

const postUser = async (body: IUser) => {
  const { data, status } = await service.postUser(body);
  console.log(body)
  return { data, status };
}

const findAllUsers = async () => {
  const users = await service.findAllUsers();
  return users;
}

const deleteUser = async (id: string) => {
  await service.deleteUser(id);
  return { status: 204 };
}

const updateUser = async (id: string, body: IUserWithouPassword) => {
  const user = await service.updateUser(id, body);
  return { data: user, status: 201};
}

export const controller = {
  getUser, getUserByEmail,
  postUser, findAllUsers,
  deleteUser, updateUser
};
