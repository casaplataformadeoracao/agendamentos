import { IEvent } from "../events/route";
import { service } from "../services/eventService";

const getEvents = async () => {
  const { data, status } = await service.getEvents();

  return { data, status };
}

const createEvent = async (body: IEvent) => {
  return await service.createEvent(body);
}

const deleteEvent = async (id: string) => {
  const deletedEvent = await service.deleteEvent(id);
  const status = deletedEvent ? 203 : 400
  return { status };
}

const updateEvent = async (id: string, body: any) => {
  const { data, status } = await service.updateEvent(id, body);
  return { data, status };
}

export const controller = { getEvents, createEvent, deleteEvent, updateEvent };