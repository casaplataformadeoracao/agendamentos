import { ILocal } from "../local/route";
import { TLocal, service } from "../services/localService";

const postLocal = async (body: ILocal) => {

  const user = await service.postLocal(body);

  return { data: user, status: 201 };
}

const getLocal = async () => {
  const local = await service.getLocal();

  return { data: local, status: 200 };
}

const deleteLocal = async (id: string) => {
  return await service.deleteLocal(id);
}

const updateLocal = async (body: TLocal, id: string) => {
  const local = await service.updateLocal(body, id);
  if (!local) {
    return { data: { message: 'Dados inválidos' }, status: 400 };
  }
  return { data: local, status: 200 };
}

export const controller = { postLocal, getLocal, deleteLocal, updateLocal };