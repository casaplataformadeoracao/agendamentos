import * as bcrypt from 'bcryptjs';
import crypto from 'crypto';

function gerarSenhaAleatoria(length: number): string {
  const caracteres = 'abcdefghijklmnopqrstuvwxyz0123456789@#$%';
  let senha = '';

  for (let i = 0; i < length; i++) {
    const indiceAleatorio = crypto.randomInt(0, caracteres.length);
    senha += caracteres.charAt(indiceAleatorio);
  }

  return senha;
}


const URL_USER = process.env.USER_ENDPOINT as string;
const URL_REGISTER = process.env.REGISTER_ENDPOINT as string;

export type ProfileRegister = {
  name: string;
  email: string;
  picture: string;
}

export const userRegister = async (email: string) => {
  const res = await fetch(URL_USER, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
    }),
  });
  const user = await res.json();

  return user;
};

export const registerFromGoogle = async (profile: ProfileRegister) => {
  const { name, email, picture } = profile;
  const res = await fetch(URL_REGISTER, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      name,
      email,
      avatar: picture,
      password: gerarSenhaAleatoria(20),  
    }),
  });
  const user = await res.json();

  return user;
}
