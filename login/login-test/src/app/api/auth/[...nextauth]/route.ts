import CredentialsProvider from 'next-auth/providers/credentials';
import GoogleProvider from 'next-auth/providers/google';
import NextAuth from 'next-auth';
import type { NextAuthOptions } from 'next-auth';
import { ProfileRegister, registerFromGoogle, userRegister } from './utils';

const URL = process.env.LOGIN_ENDPOINT as string;

const secret = process.env.NEXTAUTH_SECRET as string;

const authOptions: NextAuthOptions = {
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID as string,
      clientSecret: process.env.GOOGLE_SECRET as string,
    }),
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        email: { type: 'email', placeholder: 'E-mail', label: 'E-mail' },
        password: {
          type: 'password',
          placeholder: 'Password',
          label: 'Password',
        },
      },
      async authorize(credentials, req) {
        const res = await fetch(URL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: credentials?.email,
            password: credentials?.password,
          }),
        });
        const user = await res.json();

        if (res.ok && user) {
          return user;
        }
        return null;
      },
    }),
  ],
  pages: {
    signIn: '/auth/signin',
    signOut: '/auth/signin',
    error: '/auth/signin',
  },
  secret,
  session: {
    strategy: 'jwt',
    maxAge: 7 * 24 * 60 * 60, // 30 days
  },
  callbacks: {
    async signIn({ account, profile }) {
      if (account?.provider === 'google') {
        const user = await userRegister(profile?.email as string);
        if (user.message) {
          await registerFromGoogle(profile as ProfileRegister);
        }
      }
      return true;
    },
    async jwt({ token, user }) {
      return { ...token, ...user };
    },
    async session({ session, token }) {
      if (!('isAdmin' in token)) {
        const user = await userRegister(session.user.email as string);
        if (user) {
          token = { ...token, ...user };
        }
      }
      session.user = token as any;
      return session;
    },
  },
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST, authOptions };
