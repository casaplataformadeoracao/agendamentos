'use server';
import { IUserWithouPassword } from '@/app/utils/UserService';
import { authOptions } from '@api/auth/[...nextauth]/route';
import { controller } from '@api/controllers/userController';
import { getServerSession } from 'next-auth';
import { NextRequest, NextResponse } from 'next/server';

export async function DELETE(
  _request: NextRequest,
  { params }: { params: { id: string } }
) {
  const session = await getServerSession(authOptions);
  if (!session?.user.isAdmin) {
    return NextResponse.json({ message: 'Unauthorized' }, { status: 401 });
  }
  const { id } = params;
  const { status } = await controller.deleteUser(id);
  return NextResponse.json({ status });
}

export async function PUT(
  request: NextRequest,
  { params }: { params: { id: string } }
) {
  const session = await getServerSession(authOptions);
  if (!session?.user.isAdmin) {
    return NextResponse.json({ message: 'Unauthorized' }, { status: 401 });
  }
  const { id } = params;

  const body: IUserWithouPassword = await request.json();
  const { data, status } = await controller.updateUser(id, body);
  return NextResponse.json({ data }, { status });
}
