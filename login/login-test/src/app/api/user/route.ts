'use server';
import { NextRequest, NextResponse } from 'next/server';
import { controller } from '../controllers/userController';

interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
}

export async function POST(request: NextRequest) {
  const { email } = await request.json();
  const { data, status } = await controller.getUserByEmail({ email });
  return NextResponse.json(data, { status });
}

export async function GET() {
  const data = await controller.findAllUsers();
  return NextResponse.json(data, { status: 200 });
}
