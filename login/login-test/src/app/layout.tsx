import AppBar from '@/components/AppBar';
import Provider from '@/components/Provider';
import RenderPages from '@/components/RenderPages';
import SideMenuComponent from '@/components/SideMenuComponent';
import { Inter } from 'next/font/google';
import { SideMenuProvider } from './contexts/SideMenuProvider';
import QueryContext from './contexts/UseQueryContext';
import './globals.css';
import version from './version.json';

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
  title: 'Casa Plataforma de Oração',
  description:
    'Sistema de Agendamento para participação presencial nas reuniões e eventos da Casa Plataforma de Oração.',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <Provider>
        <QueryContext>
          <body>
            <div
              dangerouslySetInnerHTML={{
                __html: '<!-- versao: ' + version.versao + ' -->',
              }}
            />
            <SideMenuProvider>
              <AppBar />
              <SideMenuComponent />
              <RenderPages>{children}</RenderPages>
            </SideMenuProvider>
          </body>
        </QueryContext>
      </Provider>
    </html>
  );
}
