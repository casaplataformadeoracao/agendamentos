'use client';

import LocationSelector from '@/components/LocationSelector';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { SubmitHandler, useForm } from 'react-hook-form';
import Swal from 'sweetalert2';
import { api } from '../lib/api';

export interface FormState {
  fullName: string;
  email: string;
  phone: string;
  country: string;
  state: string;
  city: string;
  password: string;
  confirmPassword: string;
}

export default function Register() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
    getValues,
  } = useForm<FormState>();

  const router = useRouter();

  const onSubmit: SubmitHandler<FormState> = async (data) => {
    const { fullName, email, phone, country, state, city, password } = data;

    // try {
    //   // 8 caracteres, 1 letra maiuscula, 1 letra minuscula, 1 caracter especial
    //   const validatePassword = z.string().min(8)
    //     .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+{}\[\]:;<>,.?~\\-])[a-zA-Z\d!@#$%^&*()_+{}\[\]:;<>,.?~\\-]{8,}$/)
    //     .parse(password);

    // } catch (e) {
    //   return console.error('Senha fraca');
    // }

    const validateConfirmPassword = data.password === data.confirmPassword;

    if (!validateConfirmPassword) {
      return console.error('Senhas não conferem');
    }

    try {
      await api.post(`/api/register`, {
        name: fullName,
        email,
        phone,
        country,
        state,
        city,
        password,
        isAdmin: false,
        verified: true,
      });
      await Swal.fire('Cadastrado com sucesso');
    } catch (e) {
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Usuário já cadastrado.',
        footer: '<a href="/auth/signin">Faça seu login!</a>',
      });
    }

    try {
      await signIn('credentials', {
        email,
        password,
        callbackUrl: '/',
      });
    } catch (e) {
      return console.error('Erro ao logar usuário');
    }

    router.push('/');
  };

  return (
    <main className='flex items-center justify-center'>
      <div className='flex flex-col gap-4 w-full'>
        <h2 className='text-2xl text-center font-bold'>Sign Up</h2>

        <form
          onSubmit={handleSubmit(onSubmit)}
          className='flex flex-col gap-4 w-full'
        >
          <div className='relative pb-4'>
            <label htmlFor='fullName'>Nome Completo</label>
            <input
              type='text'
              id='fullName'
              placeholder='Nome Completo'
              className='form-control'
              {...register('fullName', { required: true })}
            />
            {errors.fullName && (
              <span className='error-message'>This field is required</span>
            )}
          </div>

          <div className='relative pb-4'>
            <label htmlFor='email'>Email</label>
            <input
              type='email'
              id='email'
              placeholder='Email'
              className='form-control'
              {...register('email', { required: true })}
            />
            {errors.email && (
              <span className='error-message'>This field is required</span>
            )}
          </div>

          <div className='relative pb-4'>
            <label htmlFor='phone'>Telefone</label>
            <input
              type='tel'
              id='phone'
              placeholder='Telefone'
              className='form-control'
              {...register('phone', { required: true })}
            />
            {errors.phone && (
              <span className='error-message'>This field is required</span>
            )}
          </div>

          <LocationSelector<FormState>
            getValues={getValues}
            register={register}
            errors={errors}
            watch={watch}
            setValue={setValue}
          />

          <div className='relative pb-4'>
            <label htmlFor='password'>Senha</label>
            <input
              type='password'
              id='password'
              placeholder='Senha'
              className='form-control'
              {...register('password', { required: true })}
            />
            {errors.password && (
              <span className='error-message'>This field is required</span>
            )}
          </div>

          <div className='relative pb-4'>
            <label htmlFor='confirmPassword'>Confirmar Senha</label>
            <input
              type='password'
              id='confirmPassword'
              placeholder='Confirmar Senha'
              className='form-control'
              {...register('confirmPassword', { required: true })}
            />
            {errors.confirmPassword && (
              <span className='error-message'>This field is required</span>
            )}
          </div>

          <div className='flex justify-center gap-4 w-full'>
            <input type='submit' value='Cadastrar' className='btn w-full' />

            <input
              type='button'
              value='Cancelar'
              className='btn w-full'
              onClick={() => router.back()}
            />
          </div>
        </form>
      </div>
    </main>
  );
}
