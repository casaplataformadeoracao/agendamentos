'use client';
import GmailButton from '@/components/GmailButton';
import Input from '@/components/Input';
import CpoLogo from '@assets/images/logo_cpo.jpg';
import { signIn, signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { MouseEventHandler, useState } from 'react';

export default function Home() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [error, setError] = useState<boolean>(false);

  const { data: session, status } = useSession();

  const validateLogin: MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault();

    const user = await signIn('credentials', {
      email,
      password,
      callbackUrl: '/',
    });
    if (user?.error) {
      setError(true);
    }
  };

  const logout: MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault();

    await signOut();
  };

  const router = useRouter();

  const register: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    router.push('/register');
  }
  
  return (
    <>
      <main className={`flex items-center justify-center gap-8 flex-wrap h-[42rem]`}>
        <form className='bg-white border border-gray-400 shadow-lg px-4 py-4 rounded-lg w-full sm:w-[20rem] h-[24.725rem]'>
          <Input
            id='email'
            type='email'
            name='email'
            data-testid='email'
            required={true}
            placeholder='E-mail'
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            key='email'
          />
          <Input
            id='password'
            type='password'
            name='password'
            required={true}
            placeholder='Password'
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            key='password'
          />
          {error && (
            <div className='flex items-center gap-2 justify-center pt-2'>
              <p className='text-red-500'>E-mail ou senha incorretos</p>
            </div>
          )}
          <div className='flex items-center gap-2 justify-center pt-2'>
            {status === 'loading' ? (
              <div className='flex items-center p-2 grid-cols-2 text-2xl'>
                Loading...
              </div>
            ) : (
              <>
                <button
                  type='button'
                  className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md'
                  data-testid='button-login'
                  onClick={validateLogin}
                >
                  Entrar
                </button>
                <button
                  className='bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-md'
                  data-testid='button-register'
                  onClick={register}
                >
                  Registrar
                </button>
              </>
            )}
            {status === 'authenticated' && (
              <button
                className='bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded'
                data-testid='button-register'
                onClick={logout}
              >
                Deslogar
              </button>
            )}
          </div>
          <fieldset className='border-t border-black mt-2'>
            <legend className='mx-auto px-4 text-black text-xl italic'>
              ou
            </legend>
            <div className='text-black pt-2'>Acesse com o seu:</div>
            <GmailButton />
          </fieldset>
        </form>
        <div className={`bg-white border border-gray-400 shadow-lg rounded-lg w-[20rem] h-[24.725rem] px-8 py-8 hidden md:flex items-center`}>
          <Image src={CpoLogo} alt="Logo CPO" className='w-[20rem]'></Image>
        </div>
      </main>
    </>
  );
}
