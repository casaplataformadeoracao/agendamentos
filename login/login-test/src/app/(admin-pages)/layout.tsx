'use server';

import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";
import { ReactNode } from "react";
import { authOptions } from "../api/auth/[...nextauth]/route";

interface ChildrenNode {
  children: ReactNode
}

export default async function LoadAdminPages({children} : ChildrenNode) {
  const session = await getServerSession(authOptions);
  if (!session?.user.isAdmin) {
    redirect("/home")
  }
  return <>{children}</>
}