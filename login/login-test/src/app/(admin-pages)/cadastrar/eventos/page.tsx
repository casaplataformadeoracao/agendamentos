'use client';
import { api } from '@/app/lib/api';
import { LocalEvent } from '@prisma/client';
import React, { useEffect } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import Swal from 'sweetalert2';
import './styles.css';

interface FormState {
  title: string;
  description: string;
  spots: number;
  date: Date;
  bookings: number;
  localEventAbbreviation: string;
}

const ReservaPalestra: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<FormState>();

  const [locais, setLocais] = React.useState<LocalEvent[]>([]);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const getLocais = async () => {
    const response = await api.get('/api/local');
    setLocais(response.data);
  };

  useEffect(() => {
    getLocais();
  }, []);

  const createEvent = async (data: FormState) => {
    await api.post('/api/events', {
      title: data.title,
      description: data.description,
      spots: +data.spots,
      date: new Date(data.date),
      bookings: +data.bookings,
      localEventAbbreviation: data.localEventAbbreviation,
    });
  };

  const onSubmit: SubmitHandler<FormState> = async (data) => {
    setIsLoading(true);

    try {
      await createEvent(data);
      Swal.fire('Cadastrado com Suceso!');
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo deu errado!',
      });
      return console.error(e);
    } finally {
      setIsLoading(false);
    }

    reset();
  };

  return (
    <main className='flex flex-col justify-center gap-4'>
      <h2 className='font-bold text-2xl'>
        Casa Plataforma de Oração - Criação de Eventos
      </h2>
      <form
        className='flex flex-col gap-4 p-4'
        onSubmit={handleSubmit(onSubmit)}
      >
        <div>
          <label htmlFor='title'>Título</label>
          <input
            id='title'
            className='form-control'
            type='text'
            {...register('title', { required: true })}
          />
          {errors.title && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        <div>
          <label htmlFor='description'>Descrição</label>
          <input
            id='description'
            className='form-control'
            type='text'
            {...register('description', { required: true })}
          />
          {errors.description && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        <div>
          <label htmlFor='vagas'>Vagas</label>
          <input
            id='vagas'
            className='form-control'
            type='number'
            {...register('spots', { required: true })}
          />
          {errors.spots && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        <div>
          <label htmlFor='date'>Data</label>
          <input
            id='date'
            className='form-control'
            type='datetime-local'
            {...register('date', { required: true })}
          />
          {errors.date && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        <div>
          <label htmlFor='reservas'>Reservas</label>
          <input
            id='reservas'
            className='form-control'
            type='number'
            {...register('bookings', { required: true })}
          />
          {errors.bookings && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        <div>
          <label htmlFor='local'>Local</label>
          <select
            id='local'
            className='form-control'
            defaultValue={''}
            {...register('localEventAbbreviation', { required: true })}
          >
            <option value='' disabled>
              Selecione um local
            </option>
            {locais.map((local: LocalEvent) => (
              <option key={local.id} value={local.abbreviation}>
                {local.localName}
              </option>
            ))}
          </select>
          {errors.localEventAbbreviation && (
            <span className='error-message'>This field is required</span>
          )}
        </div>

        {isLoading ? (
          <span>Carregando...</span>
        ) : (
          <input className='btn' type='submit' />
        )}
      </form>
    </main>
  );
};

export default ReservaPalestra;
