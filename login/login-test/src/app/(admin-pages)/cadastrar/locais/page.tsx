'use client';

import React from 'react'
import { useForm } from 'react-hook-form';
import { localService } from '@/app/utils/LocalService';
import { api } from '@/app/lib/api';
import LocationSelector from '@/components/LocationSelector';
import Swal from 'sweetalert2';

export type IFormLocal = {
  id?: number;
  name: string;
  address: string;
  addressNumber: string;
  city: string;
  state: string;
  abbreviation: string;
  country: string;
}

function CadastroLocal() {
  const {
    register,
    handleSubmit,
    reset,
    watch,
    setValue,
    getValues,
    formState: { errors }
  } = useForm<IFormLocal>()

  const onSubmit = async (data: IFormLocal) => {
    const service = new localService(api)

    try {
      await service.postLocal(data)
      await Swal.fire('Cadastrado com Sucesso')
    } catch (e) {
      await Swal.fire('Erro ao cadastrar')
    }

    reset({
      name: '',
      address: '',
      addressNumber: '',
      city: '',
      state: '',
      abbreviation: '',
      country: '',
    });
  }

  const renderAddressNumberError = () => {
    if (errors.addressNumber?.type === 'min') {
      return `O número deve ser maior que 0`
    }

    if (errors.addressNumber?.type === 'max') {
      return `O número deve ser menor que 99999`
    }

    return 'Campo obrigatório'
  }

  return (
    <main className='flex flex-col justify-center gap-4'>
      <h2 className='font-bold text-2xl'>Casa Plataforma de Oração - Criação de Locais</h2>
      < form
        onSubmit={handleSubmit(onSubmit)}
        className='flex flex-col gap-4 p-4'
      >

        <div className='relative pb-4'>
          <label htmlFor='name'>Nome</label>
          <input
            id='name'
            type='text'
            className='form-control'
            {...register("name", { required: true })}
          />
          {errors.name && <span className='error-message'>Campo obrigatório</span>}
        </div>

        <div className='relative pb-4'>
          <label htmlFor='abbreviation'>Sigla</label>
          <input
            id='abbreviation'
            type='text'
            className='form-control'
            {...register("abbreviation", { required: true })}
          />
          {errors.abbreviation && <span className='error-message'>Campo obrigatório</span>}
        </div>


        <div className='relative pb-4'>
          <label htmlFor='address'>Endereço</label>
          <input
            id='address'
            type='text'
            className='form-control'
            {...register("address", { required: true })}
          />
          {errors.address && <span className='error-message'>Campo obrigatório</span>}
        </div>

        <div className='relative pb-4'>
          <label htmlFor='addressNumber'>Número</label>
          <input
            id='addressNumber'
            type='number'
            className='form-control'
            {...register("addressNumber", { required: true, min: 0, max: 99999 })}
          />
          {errors.addressNumber && <span className='error-message'>{renderAddressNumberError()}</span>}
        </div>

        <LocationSelector<IFormLocal>
          register={register}
          setValue={setValue}
          getValues={getValues}
          watch={watch}
          errors={errors}
        />

        <input
          type="submit"
          className='btn'
          value='Cadastrar'
        />

      </form >
    </main >
  )
}

export default CadastroLocal