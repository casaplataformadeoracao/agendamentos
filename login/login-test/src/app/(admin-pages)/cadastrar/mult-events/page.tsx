'use client';
import React from 'react';
import './styles.css';
import { FileSpreadsheet  } from 'lucide-react';
import { useForm } from 'react-hook-form';
import { Button } from 'antd';
import Papa from "papaparse";

type InputFile = {
  file: File[];
}

const CreateMultipleEvents: React.FC = () => {
  const { register, handleSubmit, watch } = useForm<InputFile>();
  const handleSubmitForm = (data: InputFile) => {
    console.log(data.file[0]);
    const file = data.file[0];
    Papa.parse(file, {
      header: true,
      complete: (results: Papa.ParseResult<File[]>) => {
        console.log(results.data);
      }
    });
  }

  return (
    <main className='flex flex-col justify-center gap-4'>
      <h2 className='font-bold text-2xl'>Casa Plataforma de Oração - Criação de Eventos</h2>
      <form onSubmit={handleSubmit(handleSubmitForm)}
        className='flex flex-col gap-4 p-4'
      >
        <div>
          <label htmlFor="media"
            className='flex cursor-pointer item-center gap-1.5 text-2xl text-gray-500 hover:text-gray-600'
          >
            <FileSpreadsheet color='green' size={32} />
            Anexar arquivo de Eventos
          </label>
          <input
            type="file"
            className='invisible'
            id="media"
            accept='.csv'
            {...register('file')}
          />
         { watch('file') && <span>{`Arquivo escolhido: ` + watch('file')[0].name}</span>}
        </div>
        <Button
          htmlType="submit"
          key="submit"
          type="primary"
          className={"bg-blue-700 text-white"}
          disabled={  watch('file') ? false : true }
        >
          Enviar
        </Button>
      </form>
    </main>
  );
};

export default CreateMultipleEvents;