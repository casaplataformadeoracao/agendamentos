'use client';

import { api } from '@/app/lib/api';
import { UserService } from '@/app/utils/UserService';
import TableUsers from '@/components/TableUsers';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import Swal from 'sweetalert2';

const ListaUsuarios = () => {
  const queryClient = useQueryClient();

  const { data: users } = useQuery(['get_users'], async () => {
    try {
      const service = new UserService(api);
      const userList = await service.getUsers();
      return userList;
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',
        timer: 5000,
      })
    }
  });
 
  const { mutate: handleDelete, isError } = useMutation({
    mutationFn: async (id: number) => {
      const service = new UserService(api);
      await service.deleteUser(id);
    },
    onSuccess: () => {
      Swal.fire('Deletado com Suceso!');
      queryClient.invalidateQueries(['get_users']);
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',
      })
    },
  });

  return (
    <main className='flex flex-col w-full gap-4'>
      <h2 className='font-bold text-center text-2xl'>Visualizar Usuários</h2>
      <div className='overflow-x-auto overflow-y-auto border-collapse border border-gray-300 shadow-lg'>
        { users &&<TableUsers data={users} handleDelete={handleDelete} />}
      </div>
    </main>
  )
}

export default ListaUsuarios;