'use client';

import { api } from '@/app/lib/api';
import { EventsService } from '@/app/utils/EventsService';
import TableEvents from '@/components/TableEvents';
import React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query';
import Swal from 'sweetalert2';

const ListaEventos = () => {

  const queryClient = useQueryClient();

  const { data, isError } = useQuery(['get_events'], async () => {
    try {
      const service = new EventsService(api);
      const locais = await service.getEvents();
      return locais;
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',
        timer: 5000,
      })
    }
  });
 
  const { mutate: handleDelete } = useMutation({
    mutationFn: async (id: number) => {
      const service = new EventsService(api);
      await service.deleteEvent(id);
    },
    onSuccess: () => {
      Swal.fire('Deletado com Suceso!');
      queryClient.invalidateQueries(['get_events']);
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',
      })
    },
  });

  const confirmDelete = (id: number) => {
    Swal.fire({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, deletar!',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        handleDelete(id);
      }
    });
  }

  return (
    <main className='flex flex-col w-full gap-4'>
      <h2 className='font-bold text-center text-2xl'>Visualizar eventos cadastrados:</h2>
      <div className='overflow-x-auto overflow-y-auto border-collapse border border-gray-300 shadow-lg'>
        { isError && <span>Desculpe, ocorreu algum erro...</span>}
        { data && <TableEvents data={data} handleDelete={confirmDelete} /> }
      </div>
    </main>
  )
}

export default ListaEventos