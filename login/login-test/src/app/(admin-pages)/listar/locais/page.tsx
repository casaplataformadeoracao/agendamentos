'use client';

import { api } from '@/app/lib/api';
import { localService } from '@/app/utils/LocalService';
import TableLocais from '@/components/TableLocais';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import Swal from 'sweetalert2';

const ListarLocais = () => {
  const queryClient = useQueryClient();

  const { data } = useQuery(['get_locals'], async () => {
    try {
      const service = new localService(api);
      const locais = await service.getLocal();

      return locais;
    } catch (e) {
      console.error(e)
    }
  });
 
  const { mutate: handleDelete } = useMutation({
    mutationFn: async (id: number) => {
      const service = new localService(api);
      await service.deleteLocal(id);
    },
    onSuccess: () => {
      Swal.fire('Deletado com Suceso!');
      queryClient.invalidateQueries(['get_locals']);
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo deu errado!',
      })}
  });


  return (
    <main className='flex flex-col w-full gap-4'>
      <h2 className='font-bold text-center text-2xl'>Visualizar Locais</h2>
      <div className='overflow-x-auto overflow-y-auto border-collapse border border-gray-300 shadow-lg'>
        { data && <TableLocais data={data} handleDelete={handleDelete} /> }
      </div>
    </main>
  )
}

export default ListarLocais;