"use client";

import { QueryClient, QueryClientProvider } from "react-query"
import { ConfigProvider } from 'antd';
import pt_BR from 'antd/locale/';

const queryClient = new QueryClient()

function QueryContext ({ children}: { children: React.ReactNode }) {
  return (
    // Provide the client to your App
    <QueryClientProvider client={queryClient}>
      <ConfigProvider locale={ pt_BR }>
      { children }
      </ConfigProvider>
    </QueryClientProvider>
  )
}

export default QueryContext;