'use client';

import { useState, ReactNode } from 'react';
import { SideMenuContext } from './SideMenuContext';

export const SideMenuProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const contextValue = {
    isOpen,
    setIsOpen,
  };

  return (
    <SideMenuContext.Provider value={contextValue}>
      {children}
    </SideMenuContext.Provider>
  );

};
