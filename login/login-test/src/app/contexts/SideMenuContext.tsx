import { createContext } from 'react';

export type ISideMenuContext = {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export const SideMenuContext = createContext<ISideMenuContext>({ isOpen: false, setIsOpen: () => { } });
