import React from 'react'

const Loading = () => {
  return (
    <div className='flex pt-8'>
      <h1 className='text-3xl text-gray-700'>Carregando dados...</h1>
    </div>
  )
}

export default Loading;