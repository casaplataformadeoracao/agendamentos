import { IEvents } from "@/components/TableEvents";
import { IUser } from "@/components/TableUsers";

interface ApiService {
  get(url: string): Promise<EventsResponse>;
  delete(url: string): Promise<any>;
  put(url: string, body: any): Promise<any>;
}

type EventsResponse = {
  data: IEvents[]
}

export class EventsService {
  private api: ApiService;

  constructor(api: ApiService) {
    this.api = api;
  }

  getEvents = async () => {
    const response = await this.api.get('/api/events');
    return response.data;
  }

  deleteEvent = async (id: number) => {
    const response = await this.api.delete(`/api/events/${id}`);
    return response;
  }

  putEvent = async (id: number, body: any) => {
    await this.api.put(`/api/events/${id}`, body);
  }
}