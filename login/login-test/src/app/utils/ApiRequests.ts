export interface Countries {
  nome: string
}

export interface IState {
  id: number;
  sigla: string;
  nome: string;
}

export interface State extends IState {
  regiao: {
    id: number;
    sigla: string;
    nome: string;
  }
}

interface AxiosResponse {
  data: Countries[] | State[];
}


interface ApiService {
  get(url: string): Promise<AxiosResponse>;
}
export class ApiRequests {
  api: ApiService;

  constructor(api: ApiService) {
    this.api = api;
  }

  public getCountries = async () => {
    const response = await this.api.get(`/paises`);
    const { data } = response;
    const countries = data.map((country: Countries) => country.nome);
    return countries;
  }

  public getStates = async () => {
    const response = await this.api.get('/estados');
    const { data } = response;
    const states = data.map(({ nome, id, sigla }: any) => ({ nome, id, sigla }))
    const statesSorted = states.sort((a, b) => a.nome.localeCompare(b.nome))
    return statesSorted;
  }

  public getCities = async (state: string) => {
    try {
      const response = await this.api.get(`/estados/${state}/municipios`);
      const { data } = response;
      const cities = data.map((city) => city.nome);
      const citiesSorted = cities.sort((a, b) => a.localeCompare(b));
      return citiesSorted;
    } catch (error) {
      throw new Error('Não foi possível carregar as cidades');
    }
  }
}