import { ILocal } from "@/components/TableLocais";

type LocalResponse = {
  data: ILocal[];
}

interface ApiService {
  post(url: string, data: any): Promise<void>;
  get(url: string): Promise<LocalResponse>;
  delete(url: string): Promise<void>;
  put(url: string, data: any): Promise<void>;
}

export class localService {
  private api: ApiService;

  constructor(api: ApiService) {
    this.api = api;
  }

  async postLocal(data: any) {
    await this.api.post('/api/local', data);
  }

  getLocal = async () => {
    const response = await this.api.get('/api/local');
    return response.data;
  }

  deleteLocal = async (id: number) => {
    const response = await this.api.delete(`/api/local/${id}`);
    return response;
  }

  updateLocal = async (data: ILocal) => {
    const response = await this.api.put(`/api/local/${data.id}`, data);
    return response;
  }
}