import { IUser } from "@/components/TableUsers";

export interface IUserWithouPassword {
  name: string;
  email: string;
  phone: string;
  city: string;
  country: string;
  state: string;
  isAdmin: boolean;
  verified: boolean;
}

interface ApiService {
  // post(url: string, data: any): Promise<void>; 
  get(url: string): Promise<UserGetResponse>;
  delete(url: string): Promise<void>;
  put(url: string, data: IUserWithouPassword): Promise<void>;
}

type UserRespose = {
  data: IUserWithouPassword[]
}

type UserGetResponse = {
  data: IUser[]
}

export class UserService {
  private api: ApiService;

  constructor(api: ApiService) {
    this.api = api;
  }

  getUsers = async () => {
    const response = await this.api.get('/api/user');
    return response.data;
  }

  deleteUser = async (id: number) => {
    const response = await this.api.delete(`/api/user/${id}`);
    return response;
  }

  updateUser = async (id: number, data: IUserWithouPassword) => {
    const response = await this.api.put(`/api/user/${id}`, data);
    return response;
  }
}