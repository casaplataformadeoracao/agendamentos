'use client';

import ComponentInstrucoes from '@/components/InstrucoesEvento';
import { useParams, usePathname } from 'next/navigation';

const InstrucoesEvento = () => {
  const router = usePathname();
  const param = useParams();

  console.log(router);
  console.log(param);

  return (
    <div className='flex flex-col'>
      <div className='flex flex-col font-sans font-semibold items-baseline pr-[5%] pl-[5%]'>
        <h1 className='text-5xl font-bold pb-8'>Instruções de participação</h1>
        <ComponentInstrucoes>
          Sendo uma casa Universalista, a Casa Plataforma de Oração busca a
          compreensão dos princípios universais das religiões, a liberdade
          intelectual e cooperação fraterna entre os diferentes segmentos
          religiosos, com o propósito de estarmos unidos em Jesus, evoluindo
          para o mundo regenerado!
        </ComponentInstrucoes>
        <p className='text-white uppercase bg-red-600 underline flex'>
          ** A inscrição só vale para uma única pessoa, acompanhantes também
          precisarão de inscrição, inclusive crianças! **
        </p>
        <br />
        <ComponentInstrucoes>
          Recomendamos à chegada até 15:30h. O limite é 16:30h, em seguida o
          portão será fechado.
        </ComponentInstrucoes>
        <ComponentInstrucoes>
          Ao entrar DESLIGUE seu celular.
        </ComponentInstrucoes>
        <ComponentInstrucoes>
          Evite a conversação antes da subida ao salão para não desarmonizar o
          ambiente e os médiuns.
        </ComponentInstrucoes>
        <ComponentInstrucoes>
          Será disponibilizado atendimento fraterno para aqueles vindos pela
          primeira vez.
        </ComponentInstrucoes>
        <ComponentInstrucoes>
          Não compareça em caso de qualquer sintoma gripal!
        </ComponentInstrucoes>
        Pedimos que evitem roupas curtas, decotadas e transparentes.
        <ComponentInstrucoes>
          Lembrando que a subida salão é por ordem de chegada.
        </ComponentInstrucoes>
        <ComponentInstrucoes>
          Caso não possa comparecer procure cancelar até 48 horas antes da
          reunião.
        </ComponentInstrucoes>
        <br />
        <ComponentInstrucoes>Sintam-se sempre bem-vindos!</ComponentInstrucoes>
      </div>
    </div>
  );
};

export default InstrucoesEvento;
