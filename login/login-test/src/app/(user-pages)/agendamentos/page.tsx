'use client';

import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import '@/app/(admin-pages)/cadastrar/eventos/styles.css';
import { EventsService } from '@/app/utils/EventsService';
import { api } from '@/app/lib/api';
import Schedules from '@/components/Schedules';
import { IEvents } from '@/components/TableEvents';
import { LocalEvent } from '@prisma/client';
import { IUser } from '@/components/TableUsers';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import Swal from 'sweetalert2';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';

interface FormData {
  startDate: Date;
  endDate: Date;
}

interface IUserWithID extends IUser {
  id: number;
}

export interface IEvent {
  id: number;
  title: string;
  description: string;
  spots: number;
  date: Date;
  bookings: number;
  local: LocalEvent;
  participants: IUserWithID[];
  localEventAbbreviation: string;
}

const Agendamentos = () => {
  const { register, handleSubmit } = useForm<FormData>();
  const [filteredEvents, setFilteredEvents] = useState<IEvents[]>([])
  const [dates, setDates] = useState({ startDate: new Date(), endDate: new Date() });

  const [isLoading, setIsLoading] = useState(false);

  const { data: session } = useSession({
    required: true,
  });
  
  const eventUpdate = async (task: IEvent) => {
    const service = new EventsService(api);
    await service.putEvent(task.id, session?.user.id); 
  }

  // const padToTwoDigits = (number: number) => {
  //     return number.toString().padStart(2, '0');
  // }

  const reloadEvents = async (data: FormData) => {
    const service = new EventsService(api)
    const response = await service.getEvents();

    const startDate = new Date(`${data.startDate}T00:00:00`);
    const endDate = new Date(`${data.endDate}T23:59:59`);
    
    const filtered = response.filter((event) => {
        const eventDate = new Date(event.date);
        return eventDate >= startDate && eventDate <= endDate;
    })
    
    setFilteredEvents(filtered);
  }

  const onSubmit = async ({ startDate, endDate }: FormData) => {
    await reloadEvents({ startDate, endDate });
    setDates({ startDate, endDate });
  }

  const { data } = useSession({
    required: true,
  });

  const isUserRegistered = (event: IEvent): boolean => {
    if (event.spots <= 0) {
      return true;
    }

    return event.participants.some((participant) => participant.id === data?.user.id);
  };

  const setReminder = (task: any) => {
    setIsLoading(true);
    Swal.fire({
      title: 'Deseja agendar este evento?',
      showCancelButton: true,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#34D399',
      cancelButtonColor: '#EF4444',
      showLoaderOnConfirm: true,
      didClose: () => {
        setIsLoading(false);
      },
      preConfirm: async () => {
        try {
          await mutation.mutateAsync(task);
          const { endDate, startDate } = dates;
          await reloadEvents({ endDate, startDate });
          Swal.fire('Sucesso', 'O evento foi agendado com sucesso', 'success');
        } catch (error) {
          Swal.fire('Erro', 'Ocorreu um erro ao agendar o evento', 'error');
        } finally {
          setIsLoading(false);
        }
      }
    });
  }

  const queryClient = useQueryClient();

  const mutation = useMutation(eventUpdate as any, {
    onSuccess: () => {
      queryClient.invalidateQueries(['get_events']);
    },
    onError: () => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocorreu algum erro!',

      })
    },
  
  })

  return (
    <main className='w-full'>
        <h2 className='font-bold text-2xl text-center'>Agendamentos</h2>
        <form className='flex flex-col gap-4 p-4 w-2/4 mx-auto' onSubmit={handleSubmit(onSubmit)}>
            <div>
                <label htmlFor='startDate'>Data de Início</label>
                <input
                    className='form-control'
                    type='date'
                    {...register('startDate')}
                />
            </div>
            <div>
                <label htmlFor='endDate'>Data de Fim</label>
                <input
                    className='form-control'
                    type='date'
                    {...register('endDate')}
                />
            </div>
            <button
                className='btn btn-primary w-2/4 mx-auto'
                type='submit'
            >
                Buscar
            </button>
        </form>
        <Schedules
          setReminder={setReminder}
          Events={filteredEvents}
          isUserRegistered={isUserRegistered}
          isLoading={isLoading}
        />
    </main>
  )
}

export default Agendamentos;