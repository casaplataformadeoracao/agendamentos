'use client';

import CardLinks from '@/components/Links';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';

export default function Home() {
  const router = useRouter();

  const { data: session, status } = useSession({
    required: true,
    onUnauthenticated() {
      router.push('/home');
    },
  });

  return (
    <main
      className={`flex justify-center md:justify-start flex-wrap gap-4 w-full`}
    >
      {status === 'authenticated' && (
        <div className='grid lg:grid-cols-3 gap-3 md:grid-cols-2 sm:grid-cols-1'>
          {session?.user?.isAdmin && (
            <>
              <CardLinks
                title='Cadastrar evento'
                color='bg-green-400'
                href='/cadastrar/eventos'
              />
              <CardLinks
                title='Cadastar Locais'
                color='bg-yellow-400'
                href='/cadastrar/locais'
              />
              <CardLinks
                title='Listar eventos'
                color='bg-purple-400'
                href='/listar/eventos'
              />
              <CardLinks
                title='Listar usuários'
                color='bg-blue-400'
                href='/listar/usuarios'
              />
              <CardLinks
                title='Listar Locais'
                color='bg-cyan-400'
                href='/listar/locais'
              />
              <CardLinks
                title='Cadastrar múltiplos eventos'
                color='bg-rose-400'
                href='/cadastrar/mult-events'
              />
            </>
          )}
          <>
            <CardLinks title='Agendamento' color='bg-lime-400' href='/' />
            <CardLinks title='Perfil' color='bg-orange-500' href='/profile' />
            <CardLinks
              title='Instruções para o evento'
              color='bg-indigo-400'
              href='/instrucoes-evento'
            />
          </>
        </div>
      )}
    </main>
  );
}
