#!/bin/bash -xe

cd /opt/prisma
cp /opt/agendamentos/.env .
npx prisma migrate deploy
rm .env

cd /opt/agendamentos
node server.js
