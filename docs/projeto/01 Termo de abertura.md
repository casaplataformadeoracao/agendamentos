# Termo de abertura

Este documento declara a inicialização do projeto, definindo seu objetivo, escopo, etapas principais e artefatos de entrega.

## Objetivo

Implementar um sistema de agendamento de lugares e reuniões/palestras realizadas na Casa Plataforma de Oração, substituindo total ou parcialmente o trabalho manual realacionado feito pelos integrantes da casa.

A proposta é utilizar tecnologias atuais de desenvolvimento e programação para entregar um sistema funcional que sirva funções básicas de:

* Criar localidades: lugares onde determinados eventos podem ocorrer. Cada lugar será identificado pelo seu nome e sua capacidade máxima de ocupação (número de lugares/cadeiras disponíveis);
* Criar eventos: atividades abertas para determinado público, com datas e horários específicos, que precisam ser organizadas com o agendamento e reserva de lugares;
* Criar agendamentos: preencher os lugares disponíveis no evento com agendamentos individuais; interação com usuários finais para garantir um lugar no evento;
* Gerenciar agendamentos: perfil adminstrador para manutenção dos agendamentos (CRUD) e marcação de pessoas VIP; perfil usuário para criar e cancelar agendamentos individuais;
* Imprimir planilhas de eventos: lista de pessoas que fizeram o agendamento em determinado evento;

O sistema terá como objetivo técnico a opção de imprimir uma lista de convidados que agendaram seu lugar através do sistema para determinado evento.

## Alinhamento estratégico e justificativa

Com o crescimento e a expansão dos trabalhos da Casa Plataforma de Oração, as atividades de administração da casa ganham corpo e precisam ser escaladas. O trabalho de agendamento de lugares que hoje é feito manualmente poderá ser gerenciado com menos esforço pelos responsáveis por estas atividades na casa.

## Produtos esperados

Um sistema completo implementando as funcionalidades identificadas no escopo e restritas apenas pelas limitações de tempo e recursos disponíveis. O código fonte será doado à Casa Plataforma de Oração pelo grupo desenvolvedor sob licença Creative Commons e mantido aberto em repositório público, servindo dois propósitos de interesse mútuo:

* Melhorias e simplificação de processos na Casa Plataforma de Oração;
* Divulgação do trabalho do grupo de voluntários desenvolvedores, agregando valor ao seu portfolio profissional.

O sistema será desenvolvido com tecnologias conteinerizáveis, facilitando a implantação e migração do sistema para novos ambientes de hospedagem.

Conhecimento da arquitetura será repassado aos integrantes da casa para que seja possível eventuais alterações no sistema sem a dependência contínua dos desenvolvedores iniciais.

## Macroetapas

Seguem abaixo as principais etapas do projeto com as datas estimadas e efetivas de sua conclusão

* Inicialização do projeto: 28/ago/2023
* Planejamento
* Execução e monitoramento
* Entrega e conclusão
* Sustentação e correção de problemas

## Interessados

Abaixo, seguem as partes interessadas com suas atribuições dentro deste projeto:

* Sabrina Ribeiro: Cliente / Usuário Final
* Michele Trindade: Cliente / Usuário Final
* Jacyane Brandão: Cliente / Usuário Final
* Julio Henrique Morimoto: Gerente de Projeto / Administrador da Infraestrutura
* Micael Medeiros: Analista Desenvolvedor
* Thiago Mendes: Scrum Master / Tech Lead
* Emílio Butz: Analista Desenvolvedor / Programador
* Alexandre Mendes: Analista Desenvolvedor
