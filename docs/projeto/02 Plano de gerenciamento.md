# Plano de gerenciamento

## Escopo
Para implementar um sistema objetivo e simples, fecharemos o escopo com casos de uso simplificados. A [arquitetura](https://gitlab.com/casaplataformadeoracao/agendamentos/-/wikis/Projeto-CPO/Arquitetura) proposta usa linguagens de programação e frameworks atuais, que devem facilitar o repasse do sistema para outros profissionais da área de TI.

### Arquitetura

O sistema contará com autenticação baseada em OAuth (Login com Google), bem como o cadastro simples de usuários com senhas protegidas no banco de dados.

O código fonte será distribuído com a licença [Creative Commons](https://br.creativecommons.net/), hospedado abertamente como forma de divulgação do trabalho da equipe de desenvolvimento.

Para facilitar a hospedagem, o banco de dados utilizado será MySQL. Criação, alteração e atualização dos esquemas de banco serão feitos com arquivos numerados no repositório, conforme as necessidades durante o desenvolvimento.

O sistema será empacotado com imagens Docker para facilitar sua portabilidade a diversos ambientes compatíveis com tecnologia de contêineres.

### Funcionalidades

Serão implementadas funções de:

* Perfís de usuário:
  * Usuário administrador;
  * Usuário comum;
* Landing page (tela de apresentação);
* Autonomia do usuário adminstrador para:
  * Cadastro e gerenciamento de usuários: CRUD, bloqueio, VIP;
  * Cadastro e gerenciamento de localidades;
  * Cadastro e gerenciamento de reuniões para cada localidade;
  * Agendamento manual de lugares nas reuniões em prol do usuário comum;
  * Impressão/exportação da lista de presença para cada reunião;
  * Importação de uma lista de reuniões através de planilha externa;
  * Gerenciamento de reuniões cadastradas: CRUD;
* Autonomia de agendamento pelo usuário comum:
  * Cadastro de perfil via "Login com Google" ou definição de senha pessoal;
  * Criar/cancelar agendamentos;
  * Envio de email para cada agendamento com informações;
  * Tela de confirmação do agendamento;
  * Configuração do conteúdo do email e da tela de confirmação;

Usuários administradores DEVEM autenticar pelo protocolo OAuth (Login com Google), não tendo sua senha gravada no banco de dados.

O bloqueio de usuários será uma função administrativa, ou automática com 3 erros de acesso ao sistema através da senha pessoal.

Conforme acordo com os clientes do projeto, algumas funcionalidades específicas poderão ser não ser implementadas como telas de interação, mas como organização do código fonte e documentação para informar onde e como as alterações de conteúdo visual podem ser feitas.

O sistema será preparado de forma que os membros técnicos da Casa Plataforma de Oração sejam sempre capazes de realizar alterações simples e visuais, como alteração de texto, imagens, links, sem a dependência dos desenvolvedores originais. Tais alterações não precisam ser incorporadas como funcionalidades do próprio sistema em tempo de execução, mas devem ser facilmente acessíveis no código fonte para que novas versões possam ser criadas e implantadas com as alterações desejadas.

## Metodologia e execução

A equipe de desenvolvimento utilizará da metodologia Scrum para realizar iterações rápidas com os clientes, entregando um conjunto reduzido de funcionalidades a cada iteração. Cada iteração terá aproximadamente 2 ou 3 semanas, com reuniões para entrega e alinhamento das próximas etapas.

## Plano de comunicação

O código fonte será hospedado na plataforma Gitlab, onde a Casa Plataforma de Oração possui propriedade de repositórios dedicados às suas atividades de tecnologia.

Toda comunicação será feita através de e-mails, telefone ou pela comunidade interna da Casa Plataforma de Oração. As reuniões poderão ser online ou presenciais, podendo ser gravadas para reguardo e proteção de todas a partes e referência histórica. Gravações não serão divulgadas ao público sem autorização de todos os envolvidos neste projeto.

## Riscos

A entrega e sustentação limitada do sistema proposto depende, principalmente, da disponibilidade da equipe de desenvolvimento, que atua em caráter voluntário neste projeto, sem qualquer vínculo empregatício. Eventualmente, os desenvolvedores originais do projeto não estarão mais disponíveis para correção de problemas ou alterações futuras. Apesar do código fonte estar aberto e ser doado à da Casa Plataforma de Oração, consideramos a concretização deste risco como inevitável. Isto pode fazer com que o sistema fique congelado no tempo, sem mão de obra capacitada capaz de atuar na sua sustentação.

Para mitigar esse risco, durante o andamento do projeto, a equipe de desenvolvimento tentará passar o máximo possível de informações e documentações que possam facilitar a sustentação do sistema em caso de sua indisponibilidade no futuro. Para isso, o envolvimento de uma ou mais pessoas da Casa Plataforma de Oração, em caráter técnico, se faz necessário para receber este conhecimento.
