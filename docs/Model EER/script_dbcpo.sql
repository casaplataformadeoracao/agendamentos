use dbcpo;

-- Tabela de regiões
CREATE TABLE TB_regions (
    CountryID INT AUTO_INCREMENT,
    DDI INT NOT NULL,
    CountryName VARCHAR(50) NOT NULL,
    RegionalName VARCHAR(50),
    PRIMARY KEY (CountryID)
);

-- Tabela de usuários
CREATE TABLE TB_users (
    Email VARCHAR(50) NOT NULL UNIQUE,
    Fullname VARCHAR(40) NOT NULL,
    CellPhone BIGINT NOT NULL,
    CityName VARCHAR(40) NOT NULL,
    CountryID INT NOT NULL,
    UserLevel TINYINT NOT NULL DEFAULT 0,
    PasswordHash VARCHAR(60) NOT NULL,
    PRIMARY KEY (Email),
    FOREIGN KEY (CountryID) REFERENCES TB_regions(CountryID)
);
